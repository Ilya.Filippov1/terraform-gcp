#https://github.com/terraform-google-modules/terraform-google-sql-db/blob/v8.0.0/modules/postgresql/variables.tf
# documentation of the sql module and other avilable variables
output "sql_database_user_name" {
  value = var.database_user_name
}

output "sql_database_name" {
  value = var.database_name
}

output "instance_ip_address" {
  value = module.sql-db_postgresql.instance_ip_address
   description = "The IPv4 address assigned for the master instance"
}

output "generated_user_password" {
  value = module.sql-db_postgresql.generated_user_password
  description = "The auto generated default user password if not input password was provided"
  sensitive = true 
}

output "instance_name" {
  value = module.sql-db_postgresql.instance_name
   description = "The instance name for the master instance"
}
output "instance_self_link" {
  value = module.sql-db_postgresql.instance_self_link
   description = "The URI of the master instance"
}

output "instance_connection_name" {
  value = module.sql-db_postgresql.instance_connection_name
  description = "The connection name of the master instance to be used in connection strings"
}

output "instances" {
  value = module.sql-db_postgresql.instances
  description = "A list of all google_sql_database_instance resources we've created"
  sensitive = true
}

output "primary" {
  value = module.sql-db_postgresql.primary
  description = "The google_sql_database_instance resource representing the primary instance"
  sensitive = true
}

output "instance_server_ca_cert" {
  value = module.sql-db_postgresql.instance_server_ca_cert
  description = "The CA certificate information used to connect to the SQL instance via SSL"
}
