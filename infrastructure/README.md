# Infrastructure

`This directory  contains the configuration files to automatically provision infrastructure like databases.`

## scripts
The directory scripts contains the scripts to set environment variables to use the provisioned infrastructure.
All scripts from this directory will be executed in the pipeline stage "deploy". The process is, to export the required
data to local environment variables, which are substituted in the values.yaml under extraEnvVars automatically. To
achieve this, the environment variables need to be referenced in the values.yaml.

