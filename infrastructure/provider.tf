//provider definition
//google and google-beta are distinct providers, see https://registry.terraform.io/providers/hashicorp/google/latest/docs/guides/provider_versions
//the provider google-beta is required for the postgresql submodule of the terraform-google-sql module, see https://github.com/terraform-google-modules/terraform-google-sql-db/
//the provider google is required for the google storage bucket, see https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/storage_bucket
provider "google" {
  credentials = file(var.credentials)
  project     = var.project_name
  region      = var.region
}

provider "google-beta" {
  credentials = file(var.credentials)
  project     = var.project_name
  region      = var.region
}
