#variable declarations for Terraform

variable "database_version" {
  description = "The database version to use"
  type = string
  default =  "POSTGRES_11"
}

variable "database_name" {
  description = "The name of the Cloud SQL resources"
  type = string
}

variable "project_id" {
  description = "The project ID to manage the Cloud SQL resources"
  type = string
}

variable "project_name" {
  type = string
  description = "The name of the project"
}

variable "region" {
  description = "The region of the Cloud SQL resources"
  type = string
  default = "europe-west3"
}

variable "zone" {
  description = "The zone for the master instance, it should be something like: us-central1-a, us-east1-c."
  type = string
  default = "europe-west3-a"
}

variable "bucket_prefix" {
  description = "GCS prefix inside the bucket. Named states for workspaces are stored in an object called <prefix>/<name>.tfstate."
  type = string
  default = "dev"
}

variable "credentials" {
  description = "Local path to Google Cloud Platform account credentials in JSON format."
  type = string
  default = "credentials.json"
}

variable "database_user_name" {
  description = "The name of the default user for the database"
  type = string
  default = "def"
}
