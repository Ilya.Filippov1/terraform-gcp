module "sql-db_postgresql" {
  source  = "GoogleCloudPlatform/sql-db/google//modules/postgresql"
  version = "8.0.0"
  # Reference for required variables: https://github.com/terraform-google-modules/terraform-google-sql-db/blob/v8.0.0/modules/postgresql/variables.tf
  region = var.region
  database_version = var.database_version
  name = var.database_name
  project_id = var.project_id
  zone = var.zone
  deletion_protection=false
  user_name = var.database_user_name
  ip_configuration = {
    authorized_networks = [
        {
            name = "Whitelist all IPs"
            value = "0.0.0.0/0"
        },
    ]
    ipv4_enabled        = true
    private_network     = null
    require_ssl         = null
    }
  create_timeout="30m"
}
