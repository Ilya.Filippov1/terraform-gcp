#/bin/bash
# Load log library from shared pipeline

echo "Generating Google Cloud SQL environment variables..."

PORT="5432"
POSTGRES_IP_ADDRESS=$(cat ./infrastructure/metadata_provisioned_resources.json | jq -r '.instance_ip_address.value[0].ip_address')
POSTGRES_DB_URL="jdbc:postgresql://${POSTGRES_IP_ADDRESS%\"}:$PORT/postgres"
POSTGRES_USER_NAME=$(cat ./infrastructure/metadata_provisioned_resources.json | jq '.sql_database_user_name.value')
POSTGRES_USER_PASSWORD=$(cat ./infrastructure/metadata_provisioned_resources.json | jq '.generated_user_password.value')

echo "POSTGRES_DB_URL=$POSTGRES_DB_URL"
echo "POSTGRES_USER_NAME=$POSTGRES_USER_NAME"
echo "POSTGRES_USER_PASSWORD=$POSTGRES_USER_PASSWORD"

if [[ -z "${POSTGRES_USER_PASSWORD}" || -z "${POSTGRES_DB_URL}" || -z "${POSTGRES_USER_NAME}" ]]; then
  echo "A required postgres variable is missing."
  # TODO: Find out if exit(1) is the reason for the bug. Program should be exited here.
else
  echo "Database Environment Variables are set from Terraform output file."
fi

export POSTGRES_DB_URL=$IP_ADDRESS
export POSTGRES_USER_NAME=$POSTGRES_USER_NAME
export POSTGRES_USER_PASSWORD=$POSTGRES_USER_PASSWORD

# Move to Maven pipeleine fragment once functionality is verified.
if [[ -z "${POSTGRES_USER_PASSWORD}" || -z "${POSTGRES_DB_URL}" || -z "${POSTGRES_USER_NAME}" ]]; then
  error "A required postgres variable is not set. Postgres spring profile has not been used."
else
  export SPRING_PROFILE="postgres"
  echo "SPRING_PROFILE is set to $SPRING_PROFILE"
fi